# Battle Ships

Battle Ships recreated in JavaFX as a school project.

Not finished, singleplayer playable with bugs, multiplayer not working.

### Features planned by school:
    - Singleplayer games
    - Multiplayer games running on a server
    - Games can be paused and saved
    - Highscores managed by the server,
        viewable on the client
    - For best grade:
        tournament system

### Features done by us:
    - Singleplayer games (but buggy)
    - Account system

### Features we worked on:
    - Multiplayer games
    - Tournament system

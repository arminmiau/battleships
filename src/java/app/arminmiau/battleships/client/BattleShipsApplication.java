package app.arminmiau.battleships.client;

import app.arminmiau.battleships.game.BattleShipsPlayer;
import app.arminmiau.battleships.shared.BattleShipsUtil;
import app.arminmiau.battleships.users.BattleShipsUser;
import app.arminmiau.battleships.users.PlayerTitle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.*;

import static app.arminmiau.battleships.shared.BattleShipsUtil.client_logger;

public class BattleShipsApplication extends Application {

    private static Stage main;
    private static Parent root;

    private static SingleplayerGameManager singleplayerGameManager;

    public static void main(String[] args) throws IOException {
        LogManager.getLogManager().reset();
        BattleShipsUtil.setUpLogger(client_logger, false);
        client_logger.log(new LogRecord(Level.INFO, "Starting client"));
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        main = stage;
        root = FXMLLoader.load(BattleShipsApplication.class.getResource("titlescreen.fxml"));
        main.getIcons().add(new Image(String.valueOf(BattleShipsApplication.class.getResource("logo.png"))));
        main.setTitle("BattleShips");
        main.setScene(new Scene(root));
        main.setResizable(false);
        main.show();

        main.setOnCloseRequest(e -> {
            if (singleplayerGameManager != null) {
                singleplayerGameManager.getController().askForSave();
                singleplayerGameManager.terminateBot();
            }
            client_logger.log(new LogRecord(Level.INFO, "Exiting client"));
        });

        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            e.printStackTrace();
            client_logger.log(new LogRecord(Level.WARNING, e.toString()));
        });

        client_logger.log(new LogRecord(Level.INFO, "Client initialization done"));
    }

    public static void launchMultiplayer(Socket s) {
        client_logger.log(new LogRecord(Level.INFO, "Loading multiplayer"));
        try {
            FXMLLoader loader = new FXMLLoader(BattleShipsApplication.class.getResource("gamescreen.fxml"));
            main.getScene().setRoot(loader.load());
            GameStageController controller = loader.getController();
            controller.setPlayer(new BattleShipsPlayer(new BattleShipsUser(NetworkingUtil.usernameCache, Integer.parseInt(NetworkingUtil.getRequest(NetworkingUtil.usernameCache+"/accountlevel")), PlayerTitle.valueOf(NetworkingUtil.getRequest(NetworkingUtil.usernameCache+"/title")), 0, null), s));
            controller.init(main.getScene());
            client_logger.log(Level.INFO, "Multiplayer loaded");
        } catch (IOException e) {
            client_logger.log(new LogRecord(Level.WARNING, "Failed loading gamescreen.fxml! Exception:\n" + e.getCause()));
        }
    }

    public static void launchSingleplayer() {
        client_logger.log(new LogRecord(Level.INFO, "Loading singleplayer"));
        try {
            FXMLLoader loader = new FXMLLoader(BattleShipsApplication.class.getResource("gamescreen.fxml"));
            main.getScene().setRoot(loader.load());
            GameStageController controller = loader.getController();
            singleplayerGameManager = new SingleplayerGameManager(controller);
            new Thread(singleplayerGameManager).start();
            controller.init(main.getScene());
            client_logger.log(Level.INFO, "Singleplayer loaded");
        } catch (IOException e) {
            client_logger.log(new LogRecord(Level.WARNING, "Failed loading gamescreen.fxml! Exception:\n" + e.getCause()));
        }
    }

    public static void exitSingeplayer() {
        if (singleplayerGameManager == null) return;
        client_logger.log(new LogRecord(Level.INFO, "Exiting singleplayer"));
        singleplayerGameManager.getController().askForSave();
        singleplayerGameManager.terminateBot();
        singleplayerGameManager = null;
        changeScene("titlescreen.fxml");
    }

    static void changeScene(String fxml) {
        try {
            root = FXMLLoader.load(BattleShipsApplication.class.getResource(fxml));
            main.getScene().setRoot(root);
        } catch (IOException e) {
            client_logger.log(new LogRecord(Level.WARNING, "Failed loading " + fxml +  "! Exception:\n" + e.getCause()));
        }
    }
}
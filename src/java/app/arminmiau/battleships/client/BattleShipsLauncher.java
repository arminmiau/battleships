package app.arminmiau.battleships.client;

import java.io.IOException;

public class BattleShipsLauncher {

    /*
        **                                                                      **
        *  THIS CLASS IS USED TO ABSTRACT FXML LAUNCH IN ORDER TO BUNDLE AS JAR  *
        **                                                                      **
                                                                                    */
    public static void main(String[] args) throws IOException {
        BattleShipsApplication.main(args);
    }
}

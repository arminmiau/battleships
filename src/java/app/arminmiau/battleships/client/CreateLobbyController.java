package app.arminmiau.battleships.client;

import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class CreateLobbyController implements Initializable {

    public ComboBox<String> typeBox;

    public TextField tournamentNameField;

    public Callback<ButtonType, LobbySettings> callback;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        typeBox.setItems(FXCollections.observableArrayList("Classic", "Tournament"));
        typeBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            tournamentNameField.setDisable(!newValue.equals("Tournament"));
        });

        callback = param -> {
            if (param == ButtonType.OK) {
                return new LobbySettings(typeBox.getValue(), tournamentNameField.getText());
            }
            return null;
        };
    }
}

package app.arminmiau.battleships.client;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;

import java.net.URL;
import java.util.ResourceBundle;

public class GameLobbyController implements Initializable {

    public Canvas backgroundCanvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backgroundCanvas.getGraphicsContext2D().drawImage(new Image(GamelistController.class.getResource("gamelobby_searching.png").toString()), 0, 0);

        new Thread(() -> {
            if (NetworkingUtil.awaitGameStart()) {
                BattleShipsApplication.launchMultiplayer(NetworkingUtil.connection);
            } else {
                BattleShipsApplication.changeScene("gamelist.fxml");
            }
        }).start();
    }

    public void leave() {
        NetworkingUtil.leaveRequest();
        BattleShipsApplication.changeScene("gamelist.fxml");
    }
}

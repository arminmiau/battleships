package app.arminmiau.battleships.client;

import app.arminmiau.battleships.game.BattleShipsPlayer;
import app.arminmiau.battleships.game.ShipDirection;
import app.arminmiau.battleships.game.PlayerShip;
import app.arminmiau.battleships.game.ShipType;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class GameStageController {

    //region JavaFX injected fields
    public Canvas backgroundCanvas;
    public Pane escapeMenuPane;
    public Text notLoggedInText;
    public Text usernameText;
    public Text userTitleText;
    public Text enemyNameText;
    public Text enemyTitleText;
    public Pane prepPanelPane;
    public Button prepareButton;
    public Text yourHitsText;
    public Text yourTurnText;
    public Text enemyHitsText;
    public Text enemyTurnText;
    public Canvas yourBoardCanvas;
    public Canvas enemyBoardCanvas;
    public Canvas ship1;
    public Canvas ship2;
    public Canvas ship3;
    public Canvas ship4;
    public Canvas ship5;
    public Canvas ship6;
    public Button showEnemyBoardButton;
    public Button showYourBoardButton;
    public Canvas winnerCanvas;
    //endregion JavaFX injected fields

    //region Object fields
    private BattleShipsPlayer player;
    private boolean isPrepared = false;
    private boolean isInBattlePhase = false;
    private boolean isPaused = false;
    private boolean isSaved = false;
    private final List<Draggable> draggables = new ArrayList<>();
    //endregion Object fields

    //region Eventhandlers
    private final EventHandler<MouseEvent> onBoardClickedHandler = e -> {
        if (!isInBattlePhase || isPaused) {
            return;
        }
        int x = (int) (e.getX()/70);
        int y = (int) (e.getY()/70);

        if (!enemyBoardCanvas.isVisible()) return;
        if (player.isEnemyBoardShot(x, y)) return;

        int res = player.tryShoot(x, y);
        if (res == 3) {
            yourHitsText.setText(String.valueOf(Integer.parseInt(yourHitsText.getText()) +1));
            enemyBoardCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("hit.png").toString()), x*70, y*70);
            enemyTurnText.setVisible(false);
            yourTurnText.setVisible(false);
            gameEnd(true);
            return;
        } else if (res == -1) {
            enemyTurnText.setVisible(true);
            yourTurnText.setVisible(false);
            return;
        }
        player.onEnemyBoardShot(x, y);
        if (res == 0)
            enemyBoardCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("miss.png").toString()), x * 70, y * 70);
        else if (res > 0) {
            yourHitsText.setText(String.valueOf(Integer.parseInt(yourHitsText.getText()) +1));
            enemyBoardCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("hit.png").toString()), x * 70, y * 70);
            if (res == 2) {
                System.out.println("P2 SHIP DESTROYED");
            }
        }

        enemyTurnText.setVisible(true);
        yourTurnText.setVisible(false);

        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        showYourBoard();*/
    };

    public void onHitRecieved(int res, int[] coords) {
        yourTurnText.setVisible(true);
        enemyTurnText.setVisible(false);

        if (res > 0) {
            enemyHitsText.setText(String.valueOf(Integer.parseInt(enemyHitsText.getText()) + 1));
            yourBoardCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("hit.png").toString()), coords[0] * 70, coords[1] * 70);
            if (res > 1) {
                System.out.println("P1 SHIP DESTROYED");
                if (res > 2) {
                    gameEnd(false);
                }
            }
        } else yourBoardCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("miss.png").toString()), coords[0] * 70, coords[1] * 70);

        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }
        showEnemyBoard();*/
    }

    private class Draggable {

        private final Node target;
        private double anchorX;
        private double anchorY;
        private double mouseOffsetFromNodeZeroX;
        private double mouseOffsetFromNodeZeroY;
        private EventHandler<MouseEvent> setAnchor;
        private EventHandler<MouseEvent> updatePositionOnDrag;
        private EventHandler<MouseEvent> commitPositionOnRelease;
        EventHandler<KeyEvent> rotateKeyPressed;
        private final int ACTIVE = 1;
        private final int INACTIVE = 0;
        private int cycleStatus = INACTIVE;
        private BooleanProperty isDraggable;
        public Draggable(Node target) {
            this(target, false);
        }
        public Draggable(Node target, boolean isDraggable) {
            this.target = target;
            createHandlers();
            createDraggableProperty();
            this.isDraggable.set(isDraggable);
        }
        private void createHandlers() {
            setAnchor = event -> {
                if (event.isPrimaryButtonDown()) {
                    cycleStatus = ACTIVE;
                    anchorX = event.getSceneX();
                    anchorY = event.getSceneY();
                    mouseOffsetFromNodeZeroX = event.getX();
                    mouseOffsetFromNodeZeroY = event.getY();
                }
                if (event.isSecondaryButtonDown()) {
                    cycleStatus = INACTIVE;
                    target.setTranslateX(0);
                    target.setTranslateY(0);
                }
            };
            updatePositionOnDrag = event -> {
                if (cycleStatus != INACTIVE) {
                    target.setTranslateX(event.getSceneX() - anchorX);
                    target.setTranslateY(event.getSceneY() - anchorY);
                }
            };
            commitPositionOnRelease = event -> {
                if (cycleStatus != INACTIVE) {
                    if (!(event.getSceneX() - mouseOffsetFromNodeZeroX >= yourBoardCanvas.getLayoutX() - 20) || !(event.getSceneX() - mouseOffsetFromNodeZeroX <= yourBoardCanvas.getLayoutX() - 50 + yourBoardCanvas.getWidth())
                            || !(event.getSceneY() - mouseOffsetFromNodeZeroY >= yourBoardCanvas.getLayoutY() - 20) || !(event.getSceneY() - mouseOffsetFromNodeZeroY <= yourBoardCanvas.getLayoutY() - 50 + yourBoardCanvas.getHeight())) {
                        cycleStatus = INACTIVE;
                        target.setTranslateX(0);
                        target.setTranslateY(0);
                        return;
                    }
                    int x = (int) (event.getSceneX() - mouseOffsetFromNodeZeroX + 35 - yourBoardCanvas.getLayoutX()) / 70;
                    int y = (int) (event.getSceneY() - mouseOffsetFromNodeZeroY + 35 - yourBoardCanvas.getLayoutY()) / 70;
                    PlayerShip s = null;
                    ShipType st = null;
                    switch (target.getId()) {
                        case "ship1", "ship2" -> st = ShipType.P;
                        case "ship3" -> st = ShipType.D;
                        case "ship4" -> st = ShipType.B;
                        case "ship5" -> st = ShipType.S;
                        case "ship6" -> st = ShipType.C;
                    }
                    switch ((int) target.getRotate()) {
                        case 0 -> s = new PlayerShip(target.getId(), st, x, y, ShipDirection.DOWN);
                        case 90 -> s = new PlayerShip(target.getId(), st, x, y, ShipDirection.RIGHT);
                        case 180 -> s = new PlayerShip(target.getId(), st, x, y + 1, ShipDirection.UP);
                        case 270 -> s = new PlayerShip(target.getId(), st, x + 1, y, ShipDirection.LEFT);
                    }

                    if (s != null && !player.addShip(s)) {
                        cycleStatus = INACTIVE;
                        target.setTranslateX(0);
                        target.setTranslateY(0);
                        return;
                    }
                    if (target.getRotate() == 0 || target.getRotate() == 180) {
                        target.setLayoutX(yourBoardCanvas.getLayoutX() + x * 70);
                        target.setLayoutY(yourBoardCanvas.getLayoutY() + y * 70);
                    } else {
                        target.setLayoutX(yourBoardCanvas.getLayoutX() + x * 70 + 35);
                        target.setLayoutY(yourBoardCanvas.getLayoutY() + y * 70 - 35);
                    }

                    target.setTranslateX(0);
                    target.setTranslateY(0);
                    cycleStatus = INACTIVE;
                }
            };
            rotateKeyPressed = e -> {
                if (e.getCode().equals(KeyCode.R) && cycleStatus == ACTIVE) {
                    if (target.getRotate() == 270) {
                        target.setRotate(0);
                        return;
                    }
                    target.setRotate(target.getRotate()+90);
                }
            };
        }
        public void createDraggableProperty() {
            isDraggable = new SimpleBooleanProperty();
            isDraggable.addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    target.addEventFilter(MouseEvent.MOUSE_PRESSED, setAnchor);
                    target.addEventFilter(MouseEvent.MOUSE_DRAGGED, updatePositionOnDrag);
                    target.addEventFilter(MouseEvent.MOUSE_RELEASED, commitPositionOnRelease);
                } else {
                    target.removeEventFilter(MouseEvent.MOUSE_PRESSED, setAnchor);
                    target.removeEventFilter(MouseEvent.MOUSE_DRAGGED, updatePositionOnDrag);
                    target.removeEventFilter(MouseEvent.MOUSE_RELEASED, commitPositionOnRelease);
                }
            });
        }
        public boolean isIsDraggable() {
            return isDraggable.get();
        }
        public BooleanProperty isDraggableProperty() {
            return isDraggable;
        }
        public Node getTarget() {
            return target;
        }
    }
    //endregion Eventhandlers

    public void init(Scene scene) {
        backgroundCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("gamescreen.png").toString()), 0, 0);

        showYourBoardButton.setStyle("-fx-background-color:  rgba(150, 150, 200, 0.15)");

        /* set ships */
        ship1.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("ship_2.png").toString()), 0, 0);
        draggables.add(new Draggable(ship1, true));

        ship2.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("ship_2.png").toString()), 0, 0);
        draggables.add(new Draggable(ship2, true));

        ship3.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("ship_3a.png").toString()), 0, 0);
        draggables.add(new Draggable(ship3, true));

        ship4.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("ship_3b.png").toString()), 0, 0);
        draggables.add(new Draggable(ship4, true));

        ship5.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("ship_4.png").toString()), 0, 0);
        draggables.add(new Draggable(ship5, true));

        ship6.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("ship_5.png").toString()), 0, 0);
        draggables.add(new Draggable(ship6, true));

        for (Draggable d :
                draggables) {
            scene.addEventHandler(KeyEvent.KEY_PRESSED, d.rotateKeyPressed);
        }
        /*          */

        usernameText.setText(player.getUser().getUsername());
        userTitleText.setText("LVL " + player.getUser().getAccountLevel() + " " + player.getUser().getTitle().getTitle());
        usernameText.setVisible(true);
        userTitleText.setVisible(true);
        notLoggedInText.setVisible(false);
    }

    public void pauseGame() {
        // TODO: create pause request func
    }

    public void saveGame() {
        // TODO: create save request func
    }

    public void showEscapeMenu() {
        escapeMenuPane.setVisible(true);
        yourBoardCanvas.removeEventHandler(MouseEvent.MOUSE_CLICKED, onBoardClickedHandler);
        enemyBoardCanvas.removeEventHandler(MouseEvent.MOUSE_CLICKED, onBoardClickedHandler);
    }

    public void hideEscapeMenu() {
        escapeMenuPane.setVisible(false);
        yourBoardCanvas.removeEventHandler(MouseEvent.MOUSE_CLICKED, onBoardClickedHandler);
        enemyBoardCanvas.removeEventHandler(MouseEvent.MOUSE_CLICKED, onBoardClickedHandler);
    }

    public void leaveGame() {
        player.disconnect();
    }

    public void onReadyClick() {
        if (isPrepared) {
            player.setPrepared(false);
            isPrepared = false;
            prepareButton.setStyle("-fx-background-color:  0");
            for (Draggable d :
                    draggables) {
                d.isDraggableProperty().set(true);
            }
        } else {
            if (player.getShips().size() < 6)
                return;
            isPrepared = true;
            prepareButton.setStyle("-fx-background-color:  #00CC00");
            if (player.setPrepared(true)) {
                hidePrepPanel();
                isInBattlePhase = true;
                if (player.isOnTurn()) {
                    yourTurnText.setVisible(true);
                    showEnemyBoard();
                } else enemyTurnText.setVisible(true);
            }
            for (Draggable d :
                    draggables) {
                d.isDraggableProperty().set(false);
            }
        }
    }

    public void gameEnd(boolean winner) {
        winnerCanvas.setVisible(true);
        if (winner)
            winnerCanvas.getGraphicsContext2D().drawImage(new Image(getClass().getResource("victory.png").toString()), 0, 0);
        else winnerCanvas.getGraphicsContext2D().drawImage(new Image(getClass().getResource("defeat.png").toString()), 0, 0);
    }

    public void hidePrepPanel() {
        prepPanelPane.setVisible(false);
    }

    public void showEnemyBoard() {
        if (!isPrepared) return;
        for (Draggable d :
                draggables) {
            d.getTarget().setVisible(false);
        }
        enemyBoardCanvas.setVisible(true);
        enemyBoardCanvas.setOnMouseClicked(onBoardClickedHandler);
        yourBoardCanvas.setVisible(false);
        showEnemyBoardButton.setStyle("-fx-background-color:  rgba(150, 250, 200, 0.2)");
        showYourBoardButton.setStyle("-fx-background-color:  0");
    }

    public void showYourBoard() {
        if (!isPrepared) return;
        for (Draggable d :
                draggables) {
            d.getTarget().setVisible(true);
        }
        yourBoardCanvas.setVisible(true);
        enemyBoardCanvas.setVisible(false);
        enemyBoardCanvas.removeEventHandler(MouseEvent.MOUSE_CLICKED, onBoardClickedHandler);
        showYourBoardButton.setStyle("-fx-background-color:  rgba(150, 250, 200, 0.2)");
        showEnemyBoardButton.setStyle("-fx-background-color:  0");
    }

    public void setPlayer(BattleShipsPlayer player) {
        this.player = player;
    }

    public void askForSave() {

    }
}

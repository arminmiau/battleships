package app.arminmiau.battleships.client;

import app.arminmiau.battleships.game.GameLobby;
import app.arminmiau.battleships.game.TournamentLobby;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsUtil.client_logger;

public class GamelistController implements Initializable {

    public Canvas backgroundCanvas;
    public ListView<GameLobby> lobbyListView;

    private Dialog<LobbySettings> createDialog;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backgroundCanvas.getGraphicsContext2D().drawImage(new Image(GamelistController.class.getResource("gamelist.png").toString()), 0, 0);

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("createlobby_dialog.fxml"));
            DialogPane root = loader.load();
            createDialog = new Dialog<>();
            createDialog.setDialogPane(root);
            CreateLobbyController controller = loader.getController();
            createDialog.setResultConverter(controller.callback);
        } catch (IOException ex) {
            client_logger.log(new LogRecord(Level.WARNING, "Error while initializing create dialog, Exception: " + ex.getMessage() + ex.getCause()));
        }

        Platform.runLater(() -> {
            if (NetworkingUtil.queryRequest()) {
                lobbyListView.setItems(FXCollections.observableArrayList(NetworkingUtil.queriedLobbies));
            }
        });
    }

    public void joinLobby() {
        GameLobby g = lobbyListView.getSelectionModel().selectedItemProperty().get();
        Socket s = NetworkingUtil.joinRequest(g.getId());
        if (s != null) {
            if (g instanceof TournamentLobby t) {

            } else {
                BattleShipsApplication.launchMultiplayer(s);
            }
        }
    }

    public void goBack() {
        BattleShipsApplication.changeScene("titlescreen.fxml");
    }

    public void createLobby() {
        Optional<LobbySettings> settings = createDialog.showAndWait();
        if (settings.isPresent()) {
            switch (settings.get().getType()) {
                case "Classic" -> {
                    if (NetworkingUtil.createLobby("classic")) {
                        BattleShipsApplication.changeScene("gamelobby.fxml");
                    }
                }
                case "Tournament" -> {
                    if (NetworkingUtil.createLobby("tournament:"+settings.get().getTournamentName())) {
                        BattleShipsApplication.changeScene("gamelobby.fxml");
                    }
                }
            }
        }
    }
}

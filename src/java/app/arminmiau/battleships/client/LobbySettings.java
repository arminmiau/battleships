package app.arminmiau.battleships.client;

public class LobbySettings {

    private final String type;
    private final String tournamentName;

    public LobbySettings(String type, String tournamentName) {
        this.type = type;
        this.tournamentName = tournamentName;
    }

    public String getType() {
        return type;
    }

    public String getTournamentName() {
        return tournamentName;
    }
}

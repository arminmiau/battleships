package app.arminmiau.battleships.client;

import app.arminmiau.battleships.game.GameLobby;
import app.arminmiau.battleships.shared.BattleShipsConstants;
import app.arminmiau.battleships.shared.BattleShipsEncryption;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsConstants.*;
import static app.arminmiau.battleships.shared.BattleShipsUtil.client_logger;

public class NetworkingUtil {

    private NetworkingUtil() {}

    public static String addressCache;
    public static boolean isLoggedIn;
    public static String usernameCache;
    public static String token;

    public static Socket connection;

    public static List<GameLobby> queriedLobbies;

    public static boolean awaitGameStart() {
        client_logger.log(Level.INFO, "Awaiting trigger code for game start");
        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String res = in.readLine();
            while (res == null) res = in.readLine();
            if (res.equals(TRG_GSTART)) {
                client_logger.log(Level.INFO, "Recieved game start token");
                return true;
            }
            client_logger.log(Level.WARNING, "Expected game start, illegal token recieved");
            return false;
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error");
        }
        return false;
    }

    public static String getRequest(String request) {
        try (Socket s = new Socket(addressCache, BattleShipsConstants.SERVER_PORT); PrintWriter out = new PrintWriter(s.getOutputStream(), true); BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()))) {
            out.println(REQ_GET + MSG_SEP + request + MSG_SEP + token);
            return in.readLine();
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error");
        }
        return null;
    }

    public static boolean queryRequest() {
        client_logger.log(new LogRecord(Level.INFO, "Trying to query"));
        ObjectInputStream in = null;
        try (Socket s = new Socket(addressCache, BattleShipsConstants.SERVER_PORT); PrintWriter out = new PrintWriter(s.getOutputStream(), true)) {
            out.println(REQ_QUERY + MSG_SEP + token);
            in = new ObjectInputStream(s.getInputStream());
            Object res = in.readObject();
            if (res.equals(ERR_NAUTH)) {
                client_logger.log(Level.WARNING, "Not authorized to query");
                return false;
            }
            queriedLobbies = (List<GameLobby>) res;
            client_logger.log(Level.INFO, "Query succesful");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            client_logger.log(Level.WARNING, "Connection error " + e.getMessage());
        } catch (ClassNotFoundException e) {
            client_logger.log(Level.WARNING, e.getMessage());
        } finally {
            if (in != null)
                try {
                in.close();} catch (IOException e) {client_logger.log(Level.SEVERE, "Error while closing stream!");}
        }
        return false;
    }

    public static Socket joinRequest(String id) {
        client_logger.log(new LogRecord(Level.INFO, "Trying to join lobby " + id));
        Socket s = null;
        try {
            s = new Socket(addressCache, BattleShipsConstants.SERVER_PORT);
            s.setKeepAlive(true);
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out.println(REQ_JOIN + MSG_SEP + id + MSG_SEP + token);
            String res = in.readLine();
            if (res.equals(ERR_NAUTH)) {
                client_logger.log(Level.WARNING, "Not authorized to join");
                s.close();
                out.close();
                in.close();
                return null;
            }
            if (res.equals(ERR_NFOUND)) {
                client_logger.log(Level.WARNING, "Requested lobby could not be found");
                s.close();
                out.close();
                in.close();
                return null;
            }
            if (!res.equals(RES_SJOI)) {
                client_logger.log(new LogRecord(Level.WARNING, "Illegal response code on join request"));
                s.close();
                out.close();
                in.close();
                return null;
            }
            out.close();
            in.close();
            return s;
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error " + e.getMessage());
        }
        return null;
    }


    public static boolean createLobby(String request) {
        client_logger.log(Level.INFO, "Request lobby creation");
        try {
            connection = new Socket(addressCache, BattleShipsConstants.SERVER_PORT);
            connection.setKeepAlive(true);
            PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            out.println(REQ_CREATE + MSG_SEP + request + MSG_SEP + token);
            if (!in.readLine().equals(RES_SCRE)) {
                connection.close();
                connection = null;
                client_logger.log(Level.WARNING, "Create lobby request failed");
                return false;
            }
            client_logger.log(Level.INFO, "Lobby created");
            return true;
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error");
        }
        return false;
    }

    public static void leaveRequest() {
        try (Socket s = new Socket(addressCache, BattleShipsConstants.SERVER_PORT); PrintWriter out = new PrintWriter(s.getOutputStream(), true); BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()))) {
            out.println(REQ_LEAVE + MSG_SEP + token);
            String res = in.readLine();
            switch (res) {
                case ERR_NAUTH -> client_logger.log(new LogRecord(Level.WARNING, "Not authorized"));
                case ERR_NFOUND -> client_logger.log(new LogRecord(Level.WARNING, "Requested lobby to leave not found"));
            }
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error");
        }
    }

    public static boolean register(String address, String username, String password) {
        client_logger.log(new LogRecord(Level.INFO, "Trying to register"));
        try (Socket s = new Socket(address, BattleShipsConstants.SERVER_PORT); PrintWriter out = new PrintWriter(s.getOutputStream(), true); BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()))) {
            out.println(REQ_REG + MSG_SEP + username + MSG_SEP + BattleShipsEncryption.encrypt(password));
            String res = in.readLine();
            if (res.equals(ERR_UTNKN)) {
                return false;
            }
            token = res;
            isLoggedIn = true;
            addressCache = address;
            client_logger.log(new LogRecord(Level.INFO, "Registration succesful"));
            return true;
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error while registering");
            return false;
        }
    }

    public static boolean authentificate(String address, String username, String password) {
        client_logger.log(new LogRecord(Level.INFO, "Trying to authentificate"));
        try (Socket s = new Socket(address, BattleShipsConstants.SERVER_PORT); PrintWriter out = new PrintWriter(s.getOutputStream(), true); BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()))) {
            out.println(REQ_AUTH + MSG_SEP + username + MSG_SEP + BattleShipsEncryption.encrypt(password));
            String res = in.readLine();
            if (res.equals(ERR_FAIL)) {
                client_logger.log(Level.INFO, "Failed to authenticate");
                return false;
            }
            token = res;
            isLoggedIn = true;
            addressCache = address;
            client_logger.log(new LogRecord(Level.INFO, "Authentification succesful"));
            return true;
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error");
            return false;
        }
    }

    public static boolean checkServer(String address) {
        client_logger.log(new LogRecord(Level.INFO, "Trying to connect to " + address));
        try (Socket s = new Socket(address, BattleShipsConstants.SERVER_PORT); PrintWriter out = new PrintWriter(s.getOutputStream(), true); BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()))) {
            out.println(REQ_TCON);
            if (in.readLine().equals(RES_SCON)) {
                client_logger.log(Level.INFO, "Connection succesful");
                return true;
            }
            client_logger.log(Level.WARNING, "Illegal response from connection");
            return false;
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection failed");
            return false;
        }
    }
}

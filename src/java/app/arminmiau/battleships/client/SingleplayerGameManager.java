package app.arminmiau.battleships.client;

import app.arminmiau.battleships.game.*;
import app.arminmiau.battleships.users.BattleShipsUser;
import app.arminmiau.battleships.users.PlayerTitle;

import java.util.Random;

public class SingleplayerGameManager implements Runnable {

    private BattleShipsGame game;
    private GameStageController controller;
    private BattleShipsPlayer player;
    private BattleShipsPlayer bot = new BattleShipsPlayer(BattleShipsUser.nullUser(), this);

    private boolean botTerminated = false;

    public SingleplayerGameManager(GameStageController controller) {
        this.controller = controller;
    }

    public BattleShipsGame getGame() {
        return game;
    }

    public GameStageController getController() {
        return controller;
    }

    @Override
    public void run() {
        // TODO: Retrieve logged in user
        player = new BattleShipsPlayer(new BattleShipsUser("sus", "amogus"), this);
        game = new BattleShipsGame(player, bot);
        controller.setPlayer(player);

        setBotShips();
        boolean battlePhase = false;
        while (!battlePhase && !botTerminated) battlePhase = bot.setPrepared(true);
        botShootLoop();
    }

    private void botShootLoop() {
        Random r = new Random();
        boolean done = false;
        while (!done && !botTerminated) {
            int x = r.nextInt(9);
            int y = r.nextInt(9);
            if (bot.isEnemyBoardShot(x, y)) continue;
            int res = bot.tryShoot(x, y);
            controller.showEnemyBoard();
            if (res == -1) continue;
            bot.onEnemyBoardShot(x, y);
            if (res == 3) done = true;
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void setBotShips() {
        Random r = new Random();
        boolean res = false;
        for (int i = 0; i < 2; i++) {
            while (!res && !botTerminated) {
                res = bot.addShip(new PlayerShip(""+i, ShipType.P, r.nextInt(9), r.nextInt(9), ShipDirection.values()[r.nextInt(3)]));
            }
            res = false;
        }
        while (!res && !botTerminated) {
            res = bot.addShip(new PlayerShip("3", ShipType.D, r.nextInt(9), r.nextInt(9), ShipDirection.values()[r.nextInt(3)]));
        }
        res = false;
        while (!res && !botTerminated) {
            res = bot.addShip(new PlayerShip("4", ShipType.S, r.nextInt(9), r.nextInt(9), ShipDirection.values()[r.nextInt(3)]));
        }
        res = false;
        while (!res && !botTerminated) {
            res = bot.addShip(new PlayerShip("5", ShipType.B, r.nextInt(9), r.nextInt(9), ShipDirection.values()[r.nextInt(3)]));
        }
        res = false;
        while (!res && !botTerminated) {
            res = bot.addShip(new PlayerShip("6", ShipType.C, r.nextInt(9), r.nextInt(9), ShipDirection.values()[r.nextInt(3)]));
        }
    }

    public void terminateBot() {
        botTerminated = true;
    }
}

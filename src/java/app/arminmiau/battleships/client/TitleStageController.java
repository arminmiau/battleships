package app.arminmiau.battleships.client;

import app.arminmiau.battleships.shared.BattleShipsConstants;
import app.arminmiau.battleships.users.PlayerTitle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsConstants.*;
import static app.arminmiau.battleships.shared.BattleShipsUtil.client_logger;

public class TitleStageController implements Initializable {

    public Canvas backgroundCanvas;
    public Text username;
    public Text userTitle;
    public Button loginPaneButton;
    public Label serverStatusLabel;
    public TextField usernameField;
    public Pane loginPane;
    public TextField passwordField;
    public TextField serverAddressField;
    public Label authFailLabel;
    public Button loginButton;
    public Button registerButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backgroundCanvas.getGraphicsContext2D().drawImage(new Image(GameStageController.class.getResource("titlescreen.png").toString()), 0, 0);

        serverAddressField.setOnKeyPressed(e -> {
            if (e.getCode() != KeyCode.ENTER) return;
            if (NetworkingUtil.checkServer(serverAddressField.getText())) {
                serverStatusLabel.setText("✔");
                serverStatusLabel.setStyle("-fx-text-fill: #00FF00;");
                usernameField.setDisable(false);
                passwordField.setDisable(false);
                loginButton.setDisable(false);
                registerButton.setDisable(false);
            } else {
                serverStatusLabel.setText("✖");
                serverStatusLabel.setStyle("-fx-text-fill: #FF0000;");
                usernameField.setDisable(true);
                passwordField.setDisable(true);
                loginButton.setDisable(true);
                registerButton.setDisable(true);
            }
        });

        if (NetworkingUtil.isLoggedIn) Platform.runLater(this::fetchUserData);
    }

    public void playAlone() {
        BattleShipsApplication.launchSingleplayer();
    }

    public void playOnline() {
        if (!NetworkingUtil.isLoggedIn) {
            login();
            return;
        }
        BattleShipsApplication.changeScene("gamelist.fxml");
    }

    public void showHighscores() {
        System.out.println("show highscores");
    }

    public void onExit() {
        client_logger.log(new LogRecord(Level.INFO, "Exiting client"));
        Platform.exit();
    }

    public void login() {
        loginPane.setVisible(true);
        if (NetworkingUtil.checkServer(serverAddressField.getText())) {
            serverStatusLabel.setText("✔");
            serverStatusLabel.setStyle("color: #00FF00;");
            usernameField.setDisable(false);
            passwordField.setDisable(false);
            loginButton.setDisable(false);
            registerButton.setDisable(false);
        }
    }

    public void authentificate() {
        if (NetworkingUtil.authentificate(serverAddressField.getText(), usernameField.getText(), passwordField.getText())) {
            NetworkingUtil.usernameCache = usernameField.getText();
            fetchUserData();
        } else {
            authFailLabel.setText("Check your username and password");
            authFailLabel.setVisible(true);
        }
    }

    public void register() {
        if (NetworkingUtil.register(serverAddressField.getText(), usernameField.getText(), passwordField.getText())) {
            NetworkingUtil.usernameCache = usernameField.getText();
            fetchUserData();
        } else {
            authFailLabel.setText("Username taken");
            authFailLabel.setVisible(true);
        }
    }

    private void fetchUserData() {
        loginPane.setVisible(false);
        loginPaneButton.setVisible(false);
        username.setText(NetworkingUtil.usernameCache);
        username.setVisible(true);
        String title = PlayerTitle.valueOf(NetworkingUtil.getRequest(username.getText()+"/title")).getTitle();
        String level = NetworkingUtil.getRequest(username.getText()+"/accountlevel");
        userTitle.setText("LVL " + level + " " + title);
        userTitle.setVisible(true);
    }

    public void closeLogin() {
        loginPane.setVisible(false);
    }
}

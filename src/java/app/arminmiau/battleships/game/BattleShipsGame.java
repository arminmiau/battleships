package app.arminmiau.battleships.game;

public class BattleShipsGame {

    private final BattleShipsPlayer p1;
    private final BattleShipsPlayer p2;

    private volatile BattleShipsPlayer current;
    private boolean finished;

    public BattleShipsGame(BattleShipsPlayer p1, BattleShipsPlayer p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public boolean onReadyStateChanged() {
        if (!p1.isPrepared() || !p2.isPrepared()) return false;
        current = p1;
        return true;
    }

    public synchronized int onShotFired(BattleShipsPlayer src, int[] coords) {
        if (!src.equals(current)) {
            return -1;
        }
        if (current == p1) current = p2;
        else if (current == p2) current = p1;
        int res = current.getBoard().isHit(coords[0], coords[1]) ? 1 : 0;
        if (res == 1) {
            if (current.getBoard().isShipDestroyed(current.getShipByCoords(coords))) res = 2;
            if (current.areAllShipsDestroyed()) {
                finished = true;
                System.out.println("game finished");
                current.recieveHit(3, coords);
                return 3;
            }
        }
        current.recieveHit(res, coords);
        return res;
    }

    public BattleShipsPlayer getCurrent() {
        return current;
    }

    public boolean isFinished() {
        return finished;
    }

}

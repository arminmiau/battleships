package app.arminmiau.battleships.game;

import app.arminmiau.battleships.client.BattleShipsApplication;
import app.arminmiau.battleships.client.SingleplayerGameManager;
import app.arminmiau.battleships.users.BattleShipsUser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;

import static app.arminmiau.battleships.shared.BattleShipsConstants.BOARD_SIZE;
import static app.arminmiau.battleships.shared.BattleShipsUtil.client_logger;

public class BattleShipsPlayer {

    private boolean isSingleplayer = false;
    private SingleplayerGameManager singleplayerGameManager;
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;
    private final BattleShipsUser user;
    private PlayerBoard board = new PlayerBoard();
    private final Set<PlayerShip> playerShips = new HashSet<>();
    private final boolean[][] enemyBoardShot = new boolean[BOARD_SIZE][BOARD_SIZE];
    private volatile boolean isPrepared = false;

    public BattleShipsPlayer(BattleShipsUser user, Socket socket) {
        this.user = user;
        this.socket = socket;
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            client_logger.log(Level.WARNING, "Connection error while initializing multiplayer");
        }
    }

    public BattleShipsPlayer(BattleShipsUser user, SingleplayerGameManager singleplayerGameManager) {
        this.user = user;
        this.singleplayerGameManager = singleplayerGameManager;
        isSingleplayer = true;
    }

    public int tryShoot(int x, int y) {
        if (isSingleplayer) {
            int res = singleplayerGameManager.getGame().onShotFired(this, new int[]{x, y});
            if (res == 3)
                singleplayerGameManager.getController().gameEnd(true);
            return res;
        }

        return -1;
    }

    public boolean isEnemyBoardShot(int x, int y) {
        return enemyBoardShot[x][y];
    }

    public void onEnemyBoardShot(int x, int y) {
        enemyBoardShot[x][y] = true;
    }

    public boolean addShip(PlayerShip playerShip) {
        if (board.setShip(playerShip)){
            playerShips.add(playerShip);
            return true;
        }
        return false;
    }

    public BattleShipsUser getUser() {
        return user;
    }

    public PlayerBoard getBoard() {
        return board;
    }

    public Set<PlayerShip> getShips() {
        return playerShips;
    }

    public PlayerShip getShipByCoords(int[] coords) {
        return board.getBoard()[coords[1]][coords[0]] != null ? board.getBoard()[coords[1]][coords[0]].getShip() : null;
    }

    public boolean isPrepared() {
        return isPrepared;
    }

    public boolean setPrepared(boolean prepared) {
        isPrepared = prepared;
        if (isSingleplayer) {
            return singleplayerGameManager.getGame().onReadyStateChanged();
        }
        boolean isBattlePhase = false;
        return isBattlePhase;
    }

    public boolean isSingleplayer() {
        return isSingleplayer;
    }

    public void disconnect() {
        if (isSingleplayer) {
            BattleShipsApplication.exitSingeplayer();
            return;
        }
        //send disconnect state and close socket
    }

    public boolean isOnTurn() {
        if (isSingleplayer) {
            return this.equals(singleplayerGameManager.getGame().getCurrent());
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BattleShipsPlayer that = (BattleShipsPlayer) o;
        return Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(singleplayerGameManager, user);
    }

    public boolean areAllShipsDestroyed() {
        for (PlayerShip s : playerShips) {
            if (!board.isShipDestroyed(s)) return false;
        }
        return true;
    }

    public void recieveHit(int res, int[] coords) {
        if (isSingleplayer && !user.equals(BattleShipsUser.nullUser())) {
            singleplayerGameManager.getController().onHitRecieved(res, coords);
        }
    }
}

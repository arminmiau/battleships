package app.arminmiau.battleships.game;

public class BoardCell {
    private PlayerShip playerShip;
    private boolean isHit;

    public BoardCell(PlayerShip playerShip) {
        this.playerShip = playerShip;
        isHit = false;
    }

    public PlayerShip getShip() {
        return playerShip;
    }

    public void setShip(PlayerShip playerShip) {
        this.playerShip = playerShip;
    }

    public boolean isHit() {
        return isHit;
    }

    public void setHit(boolean hit) {
        isHit = hit;
    }
}

package app.arminmiau.battleships.game;

import app.arminmiau.battleships.users.BattleShipsUser;

import java.io.Serializable;
import java.net.Socket;
import java.util.UUID;

public class GameLobby implements Serializable {

    protected final String id;
    protected final BattleShipsUser creator;

    public GameLobby(BattleShipsUser creator) {
        id = UUID.randomUUID().toString();
        this.creator = creator;
    }

    public String getId() {
        return id;
    }

    public BattleShipsUser getCreator() {
        return creator;
    }

    @Override
    public String toString() {
        return creator.getUsername() + "\t\t\t\t\t\t\t\t\t1/2\n<LVL " + creator.getAccountLevel() + " " + creator.getTitle().getTitle() + ">";
    }
}

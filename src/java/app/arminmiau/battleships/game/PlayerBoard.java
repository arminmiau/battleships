package app.arminmiau.battleships.game;

import static app.arminmiau.battleships.shared.BattleShipsConstants.BOARD_SIZE;

public class PlayerBoard {

    private BoardCell[][] board = new BoardCell[BOARD_SIZE][BOARD_SIZE];

    boolean setShip(PlayerShip playerShip) {
        int size = playerShip.getType().getSize();
        int x = playerShip.getStartX();
        int y = playerShip.getStartY();
        if (!checkInbounds(x, y)) return false;
        if (board[y][x] != null) return false;
        int x1 = x;
        int y1 = y;
        switch (playerShip.getDirection()) {
            case UP -> y1 -= playerShip.getType().getSize()-1;
            case DOWN -> y1 += playerShip.getType().getSize()-1;
            case LEFT -> x1 -= playerShip.getType().getSize()-1;
            case RIGHT -> x1 += playerShip.getType().getSize()-1;
        }
        if (!checkInbounds(x1, y1)) return false;
        int amountSet = 0;
        switch (playerShip.getDirection()) {
            case UP -> {
                for (int i = y; i >= y1; i--) {
                    if (board[i][x] != null) {
                        for (int j = y; j >= y+amountSet; j--) {
                            board[j][x] = null;
                        }
                        return false;
                    }
                    board[i][x] = new BoardCell(playerShip);
                    amountSet++;
                }
            }
            case DOWN -> {
                for (int i = y; i <= y1; i++) {
                    if (board[i][x] != null) {
                        for (int j = y; j <= y+amountSet; j++) {
                            board[j][x] = null;
                        }
                        return false;
                    }
                    board[i][x] = new BoardCell(playerShip);
                    amountSet++;
                }
            }
            case LEFT -> {
                for (int i = x; i >= x1; i--) {
                    if (board[y][i] != null) {
                        for (int j = x; j >= x+amountSet; j--) {
                            board[y][j] = null;
                        }
                        return false;
                    }
                    board[y][i] = new BoardCell(playerShip);
                    amountSet++;
                }
            }
            case RIGHT -> {
                for (int i = x; i <= x1; i++) {
                    if (board[y][i] != null) {
                        for (int j = x; j <= x+amountSet; j++) {
                            board[y][j] = null;
                        }
                        return false;
                    }
                    board[y][i] = new BoardCell(playerShip);
                    amountSet++;
                }
            }
        }
        return true;
    }

    private boolean checkInbounds(int x, int y) {
        return x >= 0 && x <= BOARD_SIZE - 1 && y >= 0 && y <= BOARD_SIZE - 1;
    }

    boolean isHit(int x, int y) {
        if (board[y][x] != null) {
            board[y][x].setHit(true);
            return true;
        }
        return false;
    }

    boolean isShipDestroyed(PlayerShip playerShip) {
        if (playerShip.isDestroyed()) return true;
        int rem = playerShip.getType().getSize();
        int x = playerShip.getStartX();
        int y = playerShip.getStartY();
        if (board[y][x].isHit()) rem--;
        for (int i = 1; i < playerShip.getType().getSize(); i++) {
            switch (playerShip.getDirection()) {
                case UP -> y--;
                case DOWN -> y++;
                case LEFT -> x--;
                case RIGHT -> x++;
            }
            if (board[y][x].isHit()) rem--;
        }
        if (rem == 0) {
            playerShip.setDestroyed(true);
            return true;
        } return false;
    }

    public BoardCell[][] getBoard() {
        return board;
    }

    public void setBoard(BoardCell[][] board) {
        this.board = board;
    }
}

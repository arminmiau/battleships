package app.arminmiau.battleships.game;

import java.util.Objects;

public class PlayerShip {

    private final String id;
    private final ShipType type;
    private final int startX;
    private final int startY;
    private final ShipDirection shipDirection;
    private boolean destroyed = false;

    public PlayerShip(String id, ShipType type, int startX, int startY, ShipDirection shipDirection) {
        this.id = id;
        this.type = type;
        this.startX = startX;
        this.startY = startY;
        this.shipDirection = shipDirection;
    }

    public ShipType getType() {
        return type;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public ShipDirection getDirection() {
        return shipDirection;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerShip playerShip = (PlayerShip) o;
        return Objects.equals(id, playerShip.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

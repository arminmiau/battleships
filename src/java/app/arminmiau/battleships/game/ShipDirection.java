package app.arminmiau.battleships.game;

public enum ShipDirection {
    UP, DOWN, LEFT, RIGHT
}

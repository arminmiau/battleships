package app.arminmiau.battleships.game;

public enum ShipType {
    P, S, D, B, C;

    public int getSize(){
        return switch (this) {
            case P -> 2;
            case S, D -> 3;
            case B -> 4;
            case C -> 5;
        };
    }
}

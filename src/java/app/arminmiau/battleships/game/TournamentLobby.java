package app.arminmiau.battleships.game;

import app.arminmiau.battleships.users.BattleShipsUser;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TournamentLobby extends GameLobby {

    private final String tournamentName;
    private final List<BattleShipsUser> participants;

    public TournamentLobby(BattleShipsUser creator, String tournamentName) {
        super(creator);
        this.tournamentName = tournamentName;
        participants = new ArrayList<>();
    }

    public void addParticipant(BattleShipsUser user) {
        participants.add(user);
    }

    public void removeParticipant(BattleShipsUser user) {
        participants.remove(user);
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public List<BattleShipsUser> getParticipants() {
        return participants;
    }

    @Override
    public String toString() {
        return tournamentName + "\t\t\t\t\t\t\t\t\t" + (participants.size()+1) + "/8\ncreated by " + creator.getUsername() + "<LVL " + creator.getAccountLevel() + " " + creator.getTitle().getTitle() + ">";
    }
}

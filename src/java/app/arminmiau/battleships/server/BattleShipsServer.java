package app.arminmiau.battleships.server;

import app.arminmiau.battleships.game.BattleShipsGame;
import app.arminmiau.battleships.shared.BattleShipsUtil;
import app.arminmiau.battleships.users.UserController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsUtil.server_logger;


public class BattleShipsServer {

    private BattleShipsServer() {}

    static boolean serverIsEnabled = true;

    private static Thread listener;

    private final static List<BattleShipsGame> games = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        List<String> argsList = List.of(args);
        LogManager.getLogManager().reset();
        BattleShipsUtil.setUpLogger(server_logger, !argsList.contains("--silent") && !argsList.contains("-s"));
        server_logger.log(new LogRecord(Level.INFO, "Starting server"));
        server_logger.log(new LogRecord(Level.INFO, "Loading users"));
        UserController.load();
        server_logger.log(new LogRecord(Level.INFO, "Start listening"));
        listener = new Thread(new ServerListener());
        listener.start();
    }

    public void changeServerEnabledStatus(boolean status) {
       if (!serverIsEnabled && status) {
           listener = new Thread(new ServerListener());
           listener.start();
           serverIsEnabled = true;
           return;
       }
       if (serverIsEnabled && !status) {
           serverIsEnabled = false;
           listener = null;
       }
    }
}

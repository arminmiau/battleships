package app.arminmiau.battleships.server;

import app.arminmiau.battleships.game.GameLobby;
import app.arminmiau.battleships.game.TournamentLobby;
import app.arminmiau.battleships.users.BattleShipsUser;

import java.net.Socket;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsUtil.client_logger;
import static app.arminmiau.battleships.shared.BattleShipsUtil.server_logger;

public class GameManager {

    private GameManager() {}

    private static final Map<String, GameLobby> openLobbies = new HashMap<>();
    private static final Map<String, Socket> lobbySockets = new HashMap<>();


    public static GameLobby getLobby(String id) {
        return openLobbies.get(id);
    }

    public static Socket getSocket(String id) {
        return lobbySockets.get(id);
    }

    public static List<GameLobby> getLobbies() {
        return List.copyOf(openLobbies.values());
    }

    public static void createLobby(BattleShipsUser user, Socket socket) {
        GameLobby g = new GameLobby(user);
        server_logger.log(new LogRecord(Level.INFO, "Created lobby " + g.getId()));
        openLobbies.put(g.getId(), g);
        lobbySockets.put(g.getId(), socket);
    }

    public static void createTournament(BattleShipsUser user, Socket socket, String tournamentName) {
        TournamentLobby t = new TournamentLobby(user, tournamentName);
        openLobbies.put(t.getId(), t);
        lobbySockets.put(t.getId(), socket);
    }

    public static void removeLobby(String id) {
        openLobbies.remove(id);
    }
}

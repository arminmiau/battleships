package app.arminmiau.battleships.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;

import static app.arminmiau.battleships.shared.BattleShipsUtil.server_logger;

public class GameWorker implements Runnable {

    private final Socket socket;

    GameWorker (Socket csocket) {
        socket = csocket;
    }

    @Override
    public void run() {
        try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true); BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            while (true) {

            }
        } catch (IOException e) {
            server_logger.log(Level.WARNING, "Connection error in GameWorker " + e.getMessage());
        }
    }
}

package app.arminmiau.battleships.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsConstants.SERVER_PORT;
import static app.arminmiau.battleships.shared.BattleShipsUtil.server_logger;

public class ServerListener implements Runnable {

    ServerListener() {}

    final List<ServerWorker> runningWorkers = new ArrayList<>();

    @Override
    public void run() {
        try (ServerSocket socket = new ServerSocket(SERVER_PORT)) {
            while (BattleShipsServer.serverIsEnabled) {
                Socket csocket = socket.accept();
                server_logger.log(new LogRecord(Level.INFO, "Connection established by " + csocket.getInetAddress()));
                ServerWorker w = new ServerWorker(csocket);
                runningWorkers.add(w);
                Thread t = new Thread(w);
                t.setDaemon(true);
                t.start();
            }
        } catch (IOException e) {
            server_logger.log(new LogRecord(Level.WARNING, e.getMessage()));
        }
    }
}

package app.arminmiau.battleships.server;

import app.arminmiau.battleships.client.SingleplayerGameManager;
import app.arminmiau.battleships.game.BattleShipsGame;
import app.arminmiau.battleships.game.BattleShipsPlayer;
import app.arminmiau.battleships.game.GameLobby;
import app.arminmiau.battleships.game.TournamentLobby;
import app.arminmiau.battleships.users.BattleShipsUser;
import app.arminmiau.battleships.users.UserController;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsConstants.*;
import static app.arminmiau.battleships.shared.BattleShipsUtil.server_logger;


public class ServerWorker implements Runnable {

    private final Socket socket;

    ServerWorker(Socket csocket) {
        socket = csocket;
    }

    @Override
    public void run() {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream())); PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
            String inMessage = in.readLine();
            String[] arguments = inMessage.split(MSG_SEP);
            switch (arguments[0]) {
                case REQ_TCON -> out.println(RES_SCON);
                case REQ_AUTH -> {
                    if (UserController.authenticate(arguments[1], arguments[2])) out.println(UserController.createToken(arguments[1]));
                    else out.println(ERR_FAIL);
                }
                case REQ_REG -> {
                    if (UserController.get(arguments[1]) == null) {
                        UserController.add(new BattleShipsUser(arguments[1], arguments[2]));
                        out.println(UserController.createToken(arguments[1]));
                        server_logger.log(new LogRecord(Level.INFO, "Created new user"));
                    } else out.println(ERR_UTNKN);
                }
                case REQ_GET -> {
                    if (UserController.isValidToken(arguments[2])) {
                        BattleShipsUser user = UserController.get(arguments[1].split("/")[0]);
                        if (user != null) {
                            switch (arguments[1].split("/")[1]) {
                                case "accountlevel" -> out.println(user.getAccountLevel());
                                case "title" -> out.println(user.getTitle().toString());
                            }
                        } else out.println(ERR_NFOUND);
                    } else out.println(ERR_NAUTH);
                }
                case REQ_QUERY -> {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    if (UserController.isValidToken(arguments[1])) {
                        oos.writeObject(GameManager.getLobbies());
                    } else oos.writeObject(ERR_NAUTH);
                    oos.flush();
                    oos.close();
                }
                case REQ_CREATE -> {
                    socket.setKeepAlive(true);
                    if (UserController.isValidToken(arguments[2])) {
                        switch (arguments[1].split(":")[0]) {
                            case "classic" -> GameManager.createLobby(UserController.getByToken(arguments[2]), socket);
                            case "tournament" -> GameManager.createTournament(UserController.getByToken(arguments[2]), socket, arguments[1].split(":")[1]);
                        }
                        out.println(RES_SCRE);
                    } else out.println(ERR_NAUTH);
                }
                case REQ_JOIN -> {
                    if (UserController.isValidToken(arguments[2])) {
                        GameLobby g = GameManager.getLobby(arguments[1]);
                        if (g != null) {
                            if (g instanceof TournamentLobby t) {
                                t.addParticipant(UserController.getByToken(arguments[2]));
                                // Refresh all participants
                            } else {
                                socket.setKeepAlive(true);
                                PrintWriter triggerOut = new PrintWriter(GameManager.getSocket(g.getId()).getOutputStream(), true);
                                triggerOut.println(TRG_GSTART);
                                triggerOut.close();
                                new BattleShipsGame(new BattleShipsPlayer(g.getCreator(), (SingleplayerGameManager) null), new BattleShipsPlayer(UserController.getByToken(arguments[2]), (SingleplayerGameManager) null));
                                new Thread(new GameWorker(GameManager.getSocket(g.getId()))).start();
                                new Thread(new GameWorker(socket)).start();
                            }
                            GameManager.removeLobby(arguments[1]);
                            out.println(RES_SJOI);
                        } else out.println(ERR_NFOUND);
                    } else out.println(ERR_NAUTH);
                }
                case REQ_LEAVE -> {
                    if (UserController.isValidToken(arguments[2])) {
                        GameLobby g = GameManager.getLobby(arguments[1]);
                        if (g != null) {
                            if (g instanceof TournamentLobby t) {
                                t.removeParticipant(UserController.getByToken(arguments[2]));
                                // Refresh all participants
                                if (t.getParticipants().size() == 0) {
                                    GameManager.removeLobby(arguments[1]);
                                }
                            } else {
                                GameManager.removeLobby(arguments[1]);
                            }
                        } else out.println(ERR_NFOUND);
                    } else out.println(ERR_NAUTH);
                }
            }
        } catch (IOException e) {
            server_logger.log(new LogRecord(Level.WARNING, e.getMessage()));
        }
    }
}

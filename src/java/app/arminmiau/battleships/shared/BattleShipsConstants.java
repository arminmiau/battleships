package app.arminmiau.battleships.shared;

import app.arminmiau.battleships.server.BattleShipsServer;

import java.time.format.DateTimeFormatter;

public class BattleShipsConstants {

    private BattleShipsConstants() {}

    public static final String FILESPATH = System.getProperty("user.home") + "/Documents/BattleShips/";
    public static final String LOG_FILEENDING = "_log.txt";
    public static final DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static final int SERVER_PORT    = 33029;       // Default tcp port

    public static final String MSG_SEP     = ";";         // Character for splitting arguments in a message

    public static final String REQ_TCON    = "TCON69";    // Request code: Try connection
    public static final String REQ_AUTH    = "AUTH";      // Request code: Request authorization
    public static final String REQ_REG     = "REG";       // Request code: Request regristration
    public static final String REQ_GET     = "GET";       // Request code: Request object of user
    public static final String REQ_QUERY   = "QUERY";     // Request code: Query for games
    public static final String REQ_CREATE  = "CREATE";    // Request code: Create new game lobby
    public static final String REQ_JOIN    = "JOIN";      // Request code: Request to join a game
    public static final String REQ_LEAVE   = "LEAVE";     // Request code: Leave the current lobby
    public static final String REQ_PLACE   = "PLACE";     // Request code: Synchronize board with server
    public static final String REQ_SHOOT   = "SHOOT";     // Request code: Process shot on server
    public static final String REQ_PAUSE   = "PAUSE";     // Request code: Ask for pausing the game
    public static final String REQ_SAVE    = "SAVE";      // Request code: Ask for saving the game

    public static final String RES_SCON    = "SCON420";   // Response code: Succesful connection
    public static final String RES_SCRE    = "SCRE";      // Response code: Succesfully created lobby
    public static final String RES_SJOI    = "SJOI";      // Response code: Succesfully joined lobby

    public static final String TRG_GSTART  = "GSTART";    // Trigger code: Opponent found, game starts

    public static final String ERR_FAIL    = "FAIL";      // Error Code: Failed authorization
    public static final String ERR_UTNKN   = "UNTKN";     // Error Code: Username taken
    public static final String ERR_NAUTH   = "NAUTH";     // Error Code: Not authorized
    public static final String ERR_NFOUND  = "NFOUND";    // Error Code: Not found

    public static final int BOARD_SIZE     = 10;           // Battleships default board size
}

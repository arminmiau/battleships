package app.arminmiau.battleships.shared;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.logging.*;

import static app.arminmiau.battleships.shared.BattleShipsConstants.*;

public class BattleShipsUtil {

    private BattleShipsUtil() {}

    public static final Logger server_logger = Logger.getLogger("server");
    public static final Logger client_logger = Logger.getLogger("client");

    public static void setUpLogger(Logger logger, boolean console) throws IOException {
        Formatter formatter = new Formatter() {
            @Override
            public String format(LogRecord arg0) {
                String b = arg0.getLevel() +
                        " " +
                        DT_FORMATTER.format(LocalDateTime.now()) +
                        "\t" +
                        arg0.getMessage() +
                        System.getProperty("line.separator");
                return b;
            }
        };

        Path logFile = Path.of(FILESPATH, logger.getName() + LOG_FILEENDING);
        if (!Files.exists(logFile.getParent())) {
            Files.createDirectory(logFile.getParent());
        }

        FileHandler fh = new FileHandler(logFile.toString());
        fh.setFormatter(formatter);
        logger.addHandler(fh);

        if (console) {
            ConsoleHandler ch = new ConsoleHandler();
            ch.setFormatter(formatter);
            logger.addHandler(ch);
        }
    }
}

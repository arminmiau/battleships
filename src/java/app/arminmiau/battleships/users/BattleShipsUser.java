package app.arminmiau.battleships.users;


import java.io.Serializable;
import java.util.Objects;

public class BattleShipsUser implements Serializable {

    private String username;
    private int accountLevel;
    private PlayerTitle title;
    private int highscore;
    private String password;

    public BattleShipsUser(String username, String password) {
        this.username = username;
        this.password = password;
        accountLevel = 1;
        title = PlayerTitle.CADET;
        highscore = 0;
    }

    public BattleShipsUser(String username, int accountLevel, PlayerTitle title, int highscore, String password) {
        this.username = username;
        this.accountLevel = accountLevel;
        this.title = title;
        this.highscore = highscore;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAccountLevel() {
        return accountLevel;
    }

    public void setAccountLevel(int accountLevel) {
        this.accountLevel = accountLevel;
    }

    public PlayerTitle getTitle() {
        return title;
    }

    public void setTitle(PlayerTitle title) {
        this.title = title;
    }

    public int getHighscore() {
        return highscore;
    }

    public void setHighscore(int highscore) {
        this.highscore = highscore;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static BattleShipsUser nullUser() {
        return new BattleShipsUser(null, 0, null, 0, null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BattleShipsUser that = (BattleShipsUser) o;
        if (username == null) return false;
        return username.equals(that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}

package app.arminmiau.battleships.users;

public enum PlayerTitle {
    CADET, LIEUTENANT;

    public String getTitle() {
        return switch (this) {
            case CADET -> "Cadet";
            case LIEUTENANT -> "Lieutenant";
        };
    }
}

package app.arminmiau.battleships.users;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static app.arminmiau.battleships.shared.BattleShipsConstants.FILESPATH;
import static app.arminmiau.battleships.shared.BattleShipsUtil.server_logger;

public class UserController {

    private static Map<String, BattleShipsUser> users = new HashMap<>();
    private static Map<String, String> stokens = new HashMap<>();

    public static void load() {
        try (ObjectInputStream file = new ObjectInputStream(new FileInputStream(FILESPATH + "users.obj"))) {
            users = (Map<String, BattleShipsUser>) file.readObject();
        } catch (IOException | ClassNotFoundException e) {
            server_logger.log(new LogRecord(Level.WARNING, "Users file not found."));
        }
    }

    public static void save() {
        try (ObjectOutputStream file = new ObjectOutputStream(new FileOutputStream(FILESPATH + "users.obj"))) {
            file.writeObject(users);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void add(BattleShipsUser user) {
        users.put(user.getUsername(), user);
        save();
    }

    public static BattleShipsUser get(String username) {
        return users.get(username);
    }

    public static void clear() {
        users = new HashMap<>();
        stokens = new HashMap<>();
        save();
    }

    public static boolean authenticate(String username, String password) {
        if (users.get(username) == null) return false;
        String realPassword = users.get(username).getPassword();
        return password.equals(realPassword);
    }

    public static String createToken(String username) {
        String token = UUID.randomUUID().toString();
        stokens.put(token, username);
        return token;
    }

    public static boolean isValidToken(String stoken) {
        return stokens.get(stoken) != null;
    }

    public static String getTokenUsername(String stoken) {
        return stokens.get(stoken);
    }

    public static BattleShipsUser getByToken(String stoken) {
        return users.get(stokens.get(stoken));
    }
}

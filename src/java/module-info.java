module app.arminmiau.battleships {
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.controls;
    requires java.logging;


    opens app.arminmiau.battleships.client to javafx.fxml, javafx.graphics;
    exports app.arminmiau.battleships.client to javafx.fxml, javafx.graphics;
    exports app.arminmiau.battleships.game;
    exports app.arminmiau.battleships.shared;
    exports app.arminmiau.battleships.users;
}